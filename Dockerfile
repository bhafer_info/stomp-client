FROM php:8.0-fpm
RUN apt-get update -y && apt-get install -y libmcrypt-dev openssl
RUN apt-get install -y vim
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /app
COPY . /app
RUN composer install
CMD php artisan serve --host=0.0.0.0 --port=8010
EXPOSE 8010

