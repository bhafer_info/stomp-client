<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\StompMessageController@stompHome')->name('welcome');
Route::post('/sendStompMessage', 'App\Http\Controllers\StompMessageController@sendStompMessage')->name('send_message');

Route::fallback(function () {
    return redirect()->route('welcome', [ 'response' => '' ]);
});
