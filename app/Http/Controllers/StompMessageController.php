<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendStompMessageRequest;
use App\Services\StompClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Stomp\Client;
use Stomp\StatefulStomp;
use Stomp\Transport\Message;
use Symfony\Component\Console\Input\Input;

class StompMessageController extends Controller
{
    public function stompHome()
    {
        request()->flush();
        return response()->view('welcome', [ 'response' => '' ]);
    }

    public function sendStompMessage(SendStompMessageRequest $request)
    {
        $stompClient = new StompClient();
        
        $payload = $stompClient->sendMessage($request);

        if (!$request->wantsJson()) {
            // preserve previous input
            request()->flash();

            return view('welcome', [
                'response' => $payload
            ]);
        } else {
            return response($payload['body'], $payload['statusCode'])->header('Content-Type', 'application/json');
        }
    }
}
