<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stomp\Client;
use Stomp\StatefulStomp;
use Stomp\Transport\Message;

class StompClient
{
    /**
     * sends a stomp message to the given queue/topic
     *
     * @param Request $request
     * @return array
     */
    public function sendMessage(Request $request) {
        $hostUrl           = 'tcp://' . trim($request->header('queue_host'));
        $client            = new Client($hostUrl);
        $client->getConnection()->setPersistentConnection(false);
        $client->setReceiptWait(7);
        $client->getConnection()->setReadTimeout($request->header('timeout'));
        $client->setLogin(
            trim($request->header('user_name')),
            trim($request->header('password'))
        );
        $queueName         = trim($request->header('queue_name'));
        $responseQueueName = trim($request->header('response_queue_name'));
        $correlationId     = trim($request->header('correlation_id'));
        $queueAddressType  = trim($request->header('queue_address_type'));
        $message           = $request->getContent();
        Log::info("Message: [ " . $message . " ]");
        $headers           = $this->getMessageHeaders($correlationId, $responseQueueName, $queueAddressType);

        if (!empty($request->input('headers')))
        {
            $headers = $this->setAdditionalHeadersFromUser($headers, $request->input('headers'));
        }


        $stomp        = new StatefulStomp($client);

        $stompMessage = new Message($message, $headers);

        try {
            $stomp->subscribe($responseQueueName, $headers['selector'], 'auto', $headers);
            $stomp->send($queueName, $stompMessage);


            if (!empty($responseQueueName))
            {
                $frame = $stomp->read();

                if (empty($frame))
                {
                    throw new \Exception('failed with empty frame');
                }
                $payload = [ 'statusCode' => 200, 'body' => $frame->getBody() ];
            } else {
                $payload = [ 'statusCode' => 200, 'body' => 'No response queue set' ];
            }
        } catch (\Exception $e) {
            $payload = [ 'statusCode' => 400, 'body' => 'Response Timeout. With exception message [ ' . $e->getMessage() . ' ]' ];
            Log::error('Stomp request failed with exception message: [ ' . $e->getMessage() . ' ]');
        }

        try {
            $stomp->unsubscribe();
            Log::info('unsubscribed successfully');
        } catch (\Exception $e) {
            // do nothing for now
            Log::error('failed to unsubscribe');
        }
        
        return $payload;
    }

    /**
     * returns default message headers
     *
     * @param $correlationId
     * @param $queueResponseName
     * @param string $queueAddressType
     * @return array
     */
    protected function getMessageHeaders($correlationId, $queueResponseName, $queueAddressType = ''): array
    {
        $headers = array(
            'destination-type'  => 'ANYCAST',   // send all messages to queue address type by default. If not set server will set multicast or queue if address is being listened by consumer.NY
            'subscription-type' => 'ANYCAST', // read from topic by default. Same as server. When not read message will be discarded after client disconnects.
            'JMSCorrelationID'  => $correlationId,
            'selector'          => "JMSCorrelationID='" . $correlationId . "'"
        );

        if (!empty($queueResponseName))
        {
            $headers['JMSReplyTo'] = 'queue://' . $queueResponseName;
        }

        return $headers;
    }

    /**
     * sets custom message headers
     *
     * @param $headers
     * @param $userSuppliedHeaders
     * @return array
     */
    protected function setAdditionalHeadersFromUser($headers, $userSuppliedHeaders): array
    {
        $userSuppliedHeaders = trim($userSuppliedHeaders);
        $userSuppliedHeaders = explode(',', $userSuppliedHeaders);
        foreach ($userSuppliedHeaders as $header)
        {
            $pieces = explode(':', $header);

            if (is_string($pieces[0]) && is_string($pieces[1]))
            {
                $headers[$pieces[0]] = $pieces[1];
            }
        }
        return $headers;
    }
}
